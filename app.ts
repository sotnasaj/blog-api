import * as http from 'http';
import * as url from 'url';
import * as dotevn from 'dotenv';

import { MongoDB } from './services/mongoDB';
import { routes } from './routes';

startApp();

async function startApp() {
    dotevn.config();
    const db = MongoDB.getInstance();

    await db.connect(process.env.MONGODB as string,
        process.env.DATABASE as string).then();

    const app = http.createServer();

    app.on("request", async (req, res) => {
        let { path } = url.parse(req.url);
        let route = routes.find((route) => {
            let methodMatch = route.method === req.method;
            let pathMatch: boolean;
            if (path === null) return;
            if (typeof (route.path) === 'string') {
                pathMatch = path === route.path;
            } else {
                pathMatch = route.path.test(path);
            }
            return methodMatch && pathMatch;
        });
        if (route === undefined) {
            res.writeHead(404, { "Content-Type": "application/json" });
            res.end(JSON.stringify({ 'message': 'resource not found' }));
            return;
        }
        try {
            route.handler(req, res);
        } catch (err) {
            console.error('Inernal error');
            res.writeHead(500, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ 'message': 'Internal error' }));
        }
    });

    app.on("error", (err) => {
        console.error("Server error");
        return null;
    });

    app.listen(process.env.PORT);
}