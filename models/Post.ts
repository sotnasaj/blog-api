export class Post {
    _id: string | undefined;
    tittle: string | undefined;
    user: string | undefined;
    synopsis: string | undefined;
    content: string | undefined;
    creationdate: Date | undefined;
    lastupdate: Date | undefined;
    tags: string[] | undefined;
    bravos: number | undefined;
    active: boolean | undefined;

    constructor({ _id, tittle, user, synopsis, content, creationdate, lastupdate,
        tags, bravos, active, }:
        {
            _id?: string, tittle?: string, user?: string, synopsis?: string,
            content?: string, creationdate?: string, lastupdate?: string,
            tags?: string[], bravos?: number, active?: boolean;
        } = {}) {
        this._id = _id;
        this.tittle = tittle;
        this.user = user;
        this.synopsis = synopsis;
        this.content = content;
        this.creationdate = new Date(creationdate as string);
        this.lastupdate = new Date(lastupdate as string);
        this.tags = tags;
        this.bravos = bravos;
        this.active = active;
    }
}

