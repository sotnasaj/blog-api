import { ObjectId } from "mongodb";

export class Comment {
    _id: ObjectId | undefined;
    content: string | undefined;
    user: string | undefined;
    creationdate: Date | undefined;
    bravos: number | undefined;
    active: boolean | undefined;

    constructor({ _id, content, user, creationdate, bravos, active, }:
        {
            _id?: string, content?: string, user?: string,
            creationdate?: string, bravos?: number, active?: boolean;

        } = {}) {
        this._id = _id ? (new ObjectId()) : (new ObjectId(_id));
        this.content = content;
        this.user = user;
        this.creationdate = new Date(creationdate as string);
        this.bravos = bravos;
        this.active = active;
    }
}