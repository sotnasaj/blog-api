import * as Joi from '@hapi/joi';

export const PostSchema = Joi.when(Joi.ref('$method'), {
    'is': 'POST',
    'then': Joi.object({

        tittle: Joi.string()
            .min(1)
            .max(120)
            .required(),

        synopsis: Joi.string()
            .min(1)
            .max(280),

        user: Joi.string()
            .min(1)
            .hex()
            .required()
            .max(24),

        content: Joi.string()
            .min(1)
            .required(),

        creationdate: Joi.date()
            .required(),

        lastupdate: Joi.date()
            .required(),

        tags: Joi.array(),
        bravos: Joi.number(),

        active: Joi.boolean()
            .default(true),

    }).error(Error('validation error')),
    'otherwise': Joi.object({

        tittle: Joi.string()
            .min(1)
            .max(120),

        synopsis: Joi.string()
            .min(1)
            .max(280),

        content: Joi.string()
            .min(1),
        lastupdate: Joi.date(),

        tags: Joi.array()
            .max(10),

        bravos: Joi.number(),

        active: Joi.boolean()
            .default(true),
    }).error(Error('validation error'))
})