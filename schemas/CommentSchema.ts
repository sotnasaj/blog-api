import * as Joi from '@hapi/joi';

export const CommentSchema = Joi.when(Joi.ref('$method'), {
    'is': 'POST',
    'then': Joi.object({

        content: Joi.string()
            .min(1)
            .required(),

        user: Joi.string()
            .min(1)
            .hex()
            .required()
            .max(24),

        creationdate: Joi.date()
            .required(),

        bravos: Joi.number(),
        active: Joi.boolean()
            .default(true),

    }).error(Error('validation error')),
    'otherwise': Joi.object({

        content: Joi.string()
            .min(1),

        bravos: Joi.number(),

        active: Joi.boolean()
            .default(true),
    }).error(Error('validation error'))
})