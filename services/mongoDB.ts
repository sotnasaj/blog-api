import { MongoClient, ObjectId, Db, ObjectID } from 'mongodb';

export class MongoDB {

    private static instance: MongoDB;
    private client: MongoClient | null = null;
    private db: Db | null = null;

    private constructor() { }

    public static getInstance() {
        if (!MongoDB.instance) {
            MongoDB.instance = new MongoDB();
        }
        return MongoDB.instance;
    }

    async connect(uri: string, db: string) {
        if (this.client) return;
        try {
            this.client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
            await this.client.connect();
            this.db = this.client.db(db);
            console.log('Conected to Mongo');
        } catch (err) {
            console.error(err);
        }
    }

    async create(collection: string, doc: any) {
        if (!this.db) throw 'No database conection';
        let result = await this.db.collection(collection).insertOne(doc);
        return result;
    }

    async findAll(collection: string) {
        if (!this.db) throw 'No database conection';
        let result = await this.db.collection(collection).find().limit(50).toArray();
        return result;
    }

    async findById(collection: string, id: string) {
        if (!this.db) throw 'No database conection';
        let result = await this.db.collection(collection).findOne({ "_id": new ObjectId(id) });
        return result;
    }

    async removeById(collection: string, id: string) {
        if (!this.db) throw 'No database conection';
        let result = await this.db.collection(collection).deleteOne({ "_id": new ObjectID(id) });
        return result;
    }

    async updateById(collection: string, id: string, doc: any) {
        if (!this.db) throw 'No database conection';
        let result = await this.db.collection(collection).updateOne(
            { '_id': new ObjectID(id) }, { '$set': doc }
        )
        return result;
    }

    async appendById(collection: string, id: string, subcollection: string, doc: any) {
        if (!this.db) throw 'No database conection';

        let result = await this.db.collection(collection).updateOne(
            { '_id': new ObjectID(id) },
            { '$push': { [subcollection]: doc } }
        )
        return result;
    }

    async detachById(collection: string, id: string, subcollection: string, subId: string) {
        if (!this.db) throw 'No database conection';
        let result = await this.db.collection(collection).updateOne({
            '_id': new ObjectId(id),
        }, {
            '$pull': { [subcollection]: { '_id': new ObjectId(subId) } }
        });
        return result;
    }
    // coming soon , edit comments and $inc for bravos (likes) both docs/subdocs
}
