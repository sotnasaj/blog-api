import * as url from 'url';

import { MongoDB } from '../services/mongoDB';
import { Post, Comment } from '../models';
import { PostSchema, CommentSchema } from '../schemas';

const db = MongoDB.getInstance();

export async function getList(req: any, res: any) {
    let result = await db.findAll('posts');
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ 'found': result }));
}

export async function getById(req: any, res: any) {
    let { query } = url.parse(req.url, true);
    let result = await db.findById('posts', query.id as string);
    if (result !== null) {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ 'found': result }));
    } else {
        res.writeHead(404, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ 'error': 'resource not found' }));
    }
}

export async function create(req: any, res: any) {
    let body: Buffer[] = [];
    req.on('data', (chunk: Buffer) => {
        body.push(chunk);
    }).on('end', async () => {
        let jsonData = JSON.parse(body.concat().toString());
        try {
            let data = await PostSchema.validateAsync(jsonData, { 'context': { 'method': req.method } });
            let result = await db.create('posts', new Post(data));
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ 'message': 'success', 'created': result.insertedId }));
        } catch (err) {
            console.error('Validation error');
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ 'message': 'bad content request', }));
        }
    })
}

export async function remove(req: any, res: any) {
    let { query } = url.parse(req.url, true);
    let result = await db.removeById('posts', query.id as string);
    if (result.deletedCount === 0 || result.deletedCount === undefined) {
        res.writeHead(404, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ 'message': 'resource not found' }));
    } else {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ 'message': 'success', 'deleted': result.deletedCount }));
    }
}

export async function update(req: any, res: any) {
    let { query } = url.parse(req.url, true);
    let body: Buffer[] = [];
    await req.on('data', (chunk: Buffer) => {
        body.push(chunk);
    }).on('end', async () => {
        let jsonData = JSON.parse(body.concat().toString());
        try {
            let data = await PostSchema.validateAsync(jsonData);
            let result = await db.updateById('posts', query.id as string, data);
            if (result.modifiedCount > 0) {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'message': 'success', 'updated': result.modifiedCount }));
            } else if (result.matchedCount > 0) {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'message': 'nothing to update' }));
            } else {
                res.writeHead(404, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'message': 'resource not found' }));
            }
        } catch (err) {
            console.error('Validation error');
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ 'message': 'bad content request', }));
        }

    })
}

export async function addComment(req: any, res: any) {
    let { query } = url.parse(req.url, true);
    let body: Buffer[] = [];
    req.on('data', (chunk: Buffer) => {
        body.push(chunk);
    }).on('end', async () => {
        let jsonData = JSON.parse(body.concat().toString());
        try {
            let data = await CommentSchema.validateAsync(jsonData, { 'context': { 'method': req.method } });
            let result = await db.appendById('posts', query.post as string, 'comments', new Comment(data));
            if (result.modifiedCount > 0) {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'message': 'success' }));
            } else {
                res.writeHead(404, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({ 'message': 'resource not found' }));
            }
        } catch (err) {
            console.error('Validation error');
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({ 'message': 'bad content request', }));
        }
    })
}

export async function removeComment(req: any, res: any) {
    let { query } = url.parse(req.url, true);
    let result = await db.detachById('posts', query.post as string, 'comments', query.comment_id as string);
    if (result.modifiedCount > 0) {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ 'message': 'success' }));
    } else {
        res.writeHead(404, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify({ 'message': 'resource not found' }));
    }
}