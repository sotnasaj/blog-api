import * as postController from './controllers/postController';

enum METHODS {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

interface IRoutes {
    path: string | RegExp;
    method: METHODS;
    handler: ((req: any, res: any) => void)
}

export const routes: IRoutes[] = [
    {
        path: '/posts',
        method: METHODS.GET,
        handler: postController.getList.bind(postController)
    },
    {
        path: /\/posts\?id=([0-9a-f]{24})/,
        method: METHODS.GET,
        handler: postController.getById.bind(postController)
    },
    {
        path: '/posts',
        method: METHODS.POST,
        handler: postController.create.bind(postController)
    },
    {
        path: /\/posts\?id=([0-9a-f]{24})/,
        method: METHODS.DELETE,
        handler: postController.remove.bind(postController)
    },
    {
        path: /\/posts\?id=([0-9a-f]{24})/,
        method: METHODS.PUT,
        handler: postController.update.bind(postController)
    },
    {
        path: /\/add-comment\?post=([0-9a-f]{24})/,
        method: METHODS.POST,
        handler: postController.addComment.bind(postController)
    },
    {
        path: /\/rm-comment\?post=([0-9a-f]{24})&comment_id=([0-9a-f]{24})/,
        method: METHODS.DELETE,
        handler: postController.removeComment.bind(postController)
    }
]