import { AlternativesSchema } from '@hapi/joi';

interface JoiContext {
    context: {
        method: string;
    }
}

export async function validateJSONS(schema: AlternativesSchema, data: any, context?: JoiContext) {
    if (context) {
        try {
            return await schema.validateAsync(data, context);
        } catch (error) {
            throw 'JoiError';
        }
    }
    try {
        return await schema.validateAsync(data);
    } catch (error) {
        throw 'JoiError';
    }
}